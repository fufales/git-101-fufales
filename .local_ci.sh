#!/bin/bash
# Copyright 2020 - Luis G. Leon Vega <luis@luisleon.me>
# Licenced under MIT
# Support: only Linux/MacOS with BASH

touch file_ok.txt
cat file_ok.txt
if [ "$?" -ne "0" ]; then
  exit 1
fi
exit 0
